import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';



(function ($) {


	// $(document).foundation();

	$(document).ready(function () {

		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit();





///scroll

$('.text-with-arrow-down').click(function () {
	$('html,body').animate({
		scrollTop: $(this).offset().top + 20
	}, 500 );
});
	

/////////////small-top-menu-oferta

$('.menu-wladze-big-option:first-child').addClass("add-underline");

$('.menu-wladze-big-option').click(function (e) {
	e.preventDefault()
	let element_id = $(this).attr('data-show-menu');
	let target = ".menu-wladze-item-" + element_id;
	$('.menu-wladze-item').hide();
	$(target).show();
	$(this).addClass("add-underline ");
	$(this).siblings().removeClass("add-underline ");
	// $(target).siblings().hide();
})
	

///////////////////white-menu

$('.card-menu>ul>li:first-child').addClass("add-arrow");

$('.card-menu>ul>li').click(function (e) {
	e.preventDefault()
	let element_id = $(this).attr('data-show-card');
	let target = ".card-item-" + element_id;
	$('.card-item ').hide();
	$(target).show();
	$(this).addClass("add-arrow");
	$(this).siblings().removeClass("add-arrow");
	// $(target).siblings().hide();
})
	
		/////////////////////////card

	

		$('.img-blank').click(function (e) {
			var bg_img = $(this).css("background-image"); 
			$(".wood-img").stop().animate({opacity: 0},300,function(){
				$('.wood-img').css("background-image", bg_img) 
						   .animate({opacity: 1},{duration:300});
			 });

		})


		///////////menu
		$('body').on('click', '#hamburger', function (e) {
			$('.menu-inside').show();
		});
		$('body').on('click', '.cross', function (e) {
			$('.menu-inside').hide();
		});

	
	});








})(jQuery);